## Fork of `ocaml-conduit`

### Fork changelog

### v4.0.1-10

Fixes the tests. They ignore the connection info.

### v4.0.1-8

Fixes build when neither `tls` nor `ocaml-ssl` is present, my also
implementing our changes in the .dummy interfaces.

### v4.0.1-6

Adds connection_info to client connection.

### v4.0.1-5

Adds connection_info to server connection callback handler.

### v4.0.1-3

Renames conduit{-xxx} to conduit{-xxx}-dream.

### v4.0.1-2

Makes sure that the tls authenticator is passed to the server functions.
Without this client auth doesn't take place even if requested.

### v4.0.1-1

The project was forked at commit 174e32067950a9293af7f4001e3c347a9eb82218.
This is upstream version 4.0.1 but might differ slightly. The `tls` dependency
is at >= 0.14.0 and there are some small changes compared to v4.0.0 involving
the dune versions and dune commands. Domain names are given as
`[ \`host ] Domain_name.t`. The authenticators and contexts are NOT lazy.

## conduit -- an OCaml network connection establishment library

[![Build Status](https://travis-ci.org/mirage/ocaml-conduit.svg?branch=master)](https://travis-ci.org/mirage/ocaml-conduit)

The `conduit` library takes care of establishing and listening for 
TCP and SSL/TLS connections for the Lwt and Async libraries.

The reason this library exists is to provide a degree of abstraction
from the precise SSL library used, since there are a variety of ways
to bind to a library (e.g. the C FFI, or the Ctypes library), as well
as well as which library is used (just OpenSSL for now).

By default, OpenSSL is used as the preferred connection library, but
you can force the use of the pure OCaml TLS stack by setting the
environment variable `CONDUIT_TLS=native` when starting your program.

The opam packages available are:

- `conduit`: the main `Conduit` module
- `conduit-lwt`: the portable Lwt implementation
- `conduit-lwt-unix`: the Lwt/Unix implementation
- `conduit-async` the Jane Street Async implementation
- `conduit-mirage`: the MirageOS compatible implementation

### Debugging

Some of the `Lwt_unix`-based modules use a non-empty `CONDUIT_DEBUG`
environment variable to output debugging information to standard error.
Just set this variable when running the program to see what URIs
are being resolved to.

### Further Informartion

* **API Docs:** https://mirage.github.io/ocaml-conduit/
* **WWW:** https://github.com/mirage/ocaml-conduit
* **E-mail:** <mirageos-devel@lists.xenproject.org>
* **Bugs:** https://github.com/mirage/ocaml-conduit/issues
