type pubkey = X509.Public_key.t

type 'a key = [
  | `Key of 'a
  | `Error of string
  | `None
]
val pp_key : pubkey key Fmt.t

type t

type keycb = unit -> pubkey key
val keycb : t -> keycb
val mk : keycb -> t

module type Internal = sig
  type t
  val key : t -> (pubkey, [ `Msg of string ]) result
end
