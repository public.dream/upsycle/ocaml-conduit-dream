type pubkey = X509.Public_key.t

type 'a key = [
  | `Key of 'a
  | `Error of string
  | `None
]

let pp_key ppf = function
  | `Key k -> Fmt.pf ppf "%a" X509.Public_key.pp k
  | `Error s -> Fmt.pf ppf "Key error: %s" s
  | `None -> Fmt.pf ppf "none"

type keycb = unit -> pubkey key

type t = Connection_info of {
  keycb : keycb;
}
let keycb (Connection_info ci) = ci.keycb
let mk keycb = Connection_info { keycb }

module type Internal = sig
  type t
  val key : t -> (pubkey, [ `Msg of string ]) result
end
