(*
 * Copyright (c) 2014 Hannes Mehnert <hannes@mehnert.org>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 *)

open Lwt.Infix

module X509_ext = X509

module X509 = struct
  let private_of_pems ~cert ~priv_key = X509_lwt.private_of_pems ~cert ~priv_key

  type authenticator = X509.Authenticator.t

  let default_authenticator =
    match Ca_certs.authenticator () with
    | Ok a -> a
    | Error (`Msg msg) -> failwith msg
end

module Conn_info : Conduit_connection_info.Internal with type t = Tls_lwt.Unix.t = struct
  type t = Tls_lwt.Unix.t
  let key t =
    match Tls_lwt.Unix.epoch t with
    | Ok ed ->
      begin match ed.peer_certificate with
      | Some c -> Ok (Tls.Core.Cert.public_key c)
      | None -> Error (`Msg "Peer has no certificate")
      end
    | Error () -> Error (`Msg "Can't get epoch data for connection")
end

module Server_specific = Conduit_lwt_server.Server_specific (Conn_info)

module Server = struct
  let init' ?backlog ?stop ?timeout tls sa callback =
    sa
    |> Conduit_lwt_server.Server_generic.listen ?backlog
    >>= Conduit_lwt_server.Server_generic.init ?stop (fun (fd, addr) ->
            Lwt.try_bind
              (fun () -> Tls_lwt.Unix.server_of_fd tls fd)
              (fun t ->
                let ic, oc = Tls_lwt.of_t t in
                Lwt.return (fd, ic, oc, t))
              (fun exn -> Lwt_unix.close fd >>= fun () -> Lwt.fail exn)
            >>= Server_specific.process_accept ?timeout (callback addr))

  let init ?backlog ~certfile ~keyfile ~authenticator ?stop ?timeout sa callback =
    X509_lwt.private_of_pems ~cert:certfile ~priv_key:keyfile
    >>= fun certificate ->
    let config = Tls.Config.server
    ~certificates:(`Single certificate)
    ~authenticator
    () in
    init' ?backlog ?stop ?timeout config sa callback
end

module Client = struct
  let connect ?src ?certificates ~authenticator host sa =
    Conduit_lwt_server.Server_generic.with_socket sa (fun fd ->
        (match src with
        | None -> Lwt.return_unit
        | Some src_sa -> Lwt_unix.bind fd src_sa)
        >>= fun () ->
        let config = Tls.Config.client ~authenticator ?certificates () in
        Lwt_unix.connect fd sa >>= fun () ->
        Tls_lwt.Unix.client_of_fd config ~host fd >|= fun t ->
        let ic, oc = Tls_lwt.of_t t in
        let keycb () = match Conn_info.key t with
        | Ok s -> `Key s
        | Error (`Msg e) -> `Error e in
        let connection_info : Conduit_connection_info.t =
          Conduit_connection_info.mk keycb in
        (fd, ic, oc, connection_info))
end

let available = true
